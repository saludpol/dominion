# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL('postgres://saludpol:y2KSalud14@localhost/dominion', pool_size=1,check_reserved=['all'])
    #db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
import datetime

now = datetime.datetime.now()

auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

auth.settings.extra_fields['auth_user']= [
Field('dni'),
Field('telefono'),
Field('direccion'),
Field('fecha_nacimiento', 'date', label='Fecha de Nacimiento')]

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

db.auth_group.update_or_insert(role='administrador')
db.auth_group.update_or_insert(role='operador')
db.auth_group.update_or_insert(role='usuario')


group_id = db.auth_group(db.auth_group.role=='usuario').id
auth.settings.create_user_groups = None
auth.settings.everybody_group_id = group_id

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login

tipos_operaciones = {
    0: T('Reclamo'),
    1: T('Sugerencia'),
}

# tipos_servicios = {
#     0: T('Administrativo'),
#     1: T('Asistencial'),
# }

generos = {
    0: T('Masculino'),
    1: T('Femenino'),
}

estados = {
    0: T('No Resuelta'),
    1: T('Proceso'),
    2: T('Resuelta Satisfactoria'),
    3: T('Resuelta Insatisfactoria')
}

db.define_table('tipos_documentos',
                Field('nombre', 'string', label=T('Nombre')),
                format='%(nombre)s'
                )

db.define_table('tipos_servicios',
                Field('nombre', 'string', label=T('Nombre')),
                format='%(nombre)s'
                )

db.define_table('tipos_usuarios',
                Field('nombre', 'string', label=T('Nombre')),
                format='%(nombre)s'
                )

db.define_table('departamentos',
                Field('nombre', 'string', label=T('Nombre')),
                Field('codigo', 'string', label=T('Codigo')),
                format='%(nombre)s'
                )

db.define_table('provincias',
                Field('nombre', 'string', label=T('Nombre')),
                Field('departamento', 'reference departamentos', label=T('Departamento')),
                Field('codigo', 'string', label=T('Codigo')),
                format='%(nombre)s'
                )

db.define_table('distritos',
                Field('nombre', 'string', label=T('Nombre')),
                Field('provincia', 'reference provincias', label=T('Provincia')),
                Field('departamento', 'reference departamentos', label=T('Departamento')),
                Field('codigo', 'string', label=T('Codigo')),
                format='%(nombre)s'
                )

db.define_table('operaciones',
                Field('registro', 'datetime', label=T('Registro'), default=now, writable=False),
                Field('registrante', 'reference auth_user', label=T('Operador')),
                Field('tipo_operacion', label=T('Tipo Operacion'), requires=IS_IN_SET(tipos_operaciones)),
                Field('origen_documento', db.tipos_documentos, label=T('Tipo Documento Derecho Habiente')),
                Field('origen_numero_documento', 'string', label=T('Numero de Documento')),
#                Field('origen_cip', 'string', label=T('Carnet de Identidad Policial')),
                Field('origen_tipo_usuario', db.tipos_usuarios, label=T('Tipo Usuario'),
                    requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_usuarios, '%(nombre)s'))),
                Field('origen_nombre', 'string', label=T('Nombre')),
#                Field('origen_sexo', label=T('Sexo'), requires=IS_IN_SET(generos)),
#                Field('origen_edad', 'integer', label=T('Edad')),
#                Field('origen_departamento', 'reference departamentos', label=T('Departamento')),
#                Field('origen_provincia', 'reference provincias', label=T('Provincia')),
#                Field('origen_distrito', 'reference distritos', label=T('Distrito')),
#                Field('origen_direccion', 'string', label=T('Direccion')),
                Field('origen_telefono_fijo', 'string', label=T('Telefono Fijo')),
                Field('origen_telefono_celular', 'string', label=T('Telefono Celular')),
                Field('origen_correo_electronico', 'string', label=T('Correo Electronico')),
                Field('tipo_servicio', 'reference tipos_servicios', label=T('Tipo Servicio'),
                    requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_servicios, '%(nombre)s'))),
                Field('registro_tipo', 'string', label=T('Persona que realiza el reclamo')),
                Field('registro_documento', db.tipos_documentos,
                      label=T('Tipo de Documento del que realiza reclamo'),
                      requires=IS_EMPTY_OR(IS_IN_DB(db, db.tipos_documentos, '%(nombre)s'))),
                Field('registro_numero_documento', 'string', label=T('Numero del Documento del que realiza reclamo')),
                Field('registro_nombre', 'string', label=T('Nombres del que realiza el reclamo')),
                Field('referencia_nombre', 'string', label=T('Nombre del Referido')),
                Field('referencia_area', 'string', label=T('Area del Referido')),
                Field('referencia_servicio', 'string', label=T('Servicio del Referido')),
#                Field('referencia_centro_asistencial', 'string', label=T('Centro Asistencial')),
                Field('resumen', 'string', label=T('Resumen')),
                Field('contenido', 'text', label=T('Contenido')),
                Field('archivo', 'upload', label=T('Archivo Adjunto')),
                Field('estado', label=T('Estado'), default=0, requires=IS_IN_SET(estados)),
                format='%(id)s - %(resumen)s'
                )

db.define_table('operaciones_acciones',
                Field('registro', 'datetime', label=T('Registro'), default=now, writable=False),
                Field('registrante', 'reference auth_user', label=T('Operador')),
                Field('operacion', 'reference operaciones', label=T('Operacion')),
                Field('contenido', 'text', label=T('Contenido')),
                Field('area', 'string', label=T('Area')),
                Field('contacto', 'string', label=T('Contacto')),
                format='%(id)s'
                )


db.operaciones.tipo_operacion.represent = lambda value, row: None if value is None else tipos_operaciones[int(value)]
db.operaciones.estado.represent = lambda value, row: None if value is None else estados[int(value)]
#db.operaciones.origen_sexo.represent = lambda value, row: None if value is None else generos[int(value)]
#db.operaciones.tipo_servicio.represent = lambda value, row: None if value is None else tipos_servicios[int(value)]
