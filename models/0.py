from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Gesti\xc3\xb3n de Quejas y Reclamos'
settings.subtitle = ''
settings.author = 'Alfonso de la Guarda Reyes'
settings.author_email = 'alfonsodg@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'd177a45d-1451-48f6-8bec-9d267104ef9e'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
