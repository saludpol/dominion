response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
home = [
(T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
]

config = [
    (T('Configuracion'), False, None,
     [
         (T('Tipos de Documentos'), False, URL('config', 'tipos_documentos'),[]),
         (T('Tipos de Usuarios'), False, URL('config', 'tipos_usuarios'),[]),
         (T('Tipos de Servicios'), False, URL('config', 'tipos_servicios'),[]),
         (T('Departamentos'), False, URL('config', 'departamentos'),[]),
         (T('Provincias'), False, URL('config', 'provincias'),[]),
         (T('Distritos'), False, URL('config', 'distritos'),[]),
         (T('Usuarios'), False, URL('config', 'users_manage'),[]),
         (T('Grupos'), False, URL('config', 'users_groups'),[]),
         (T('Membresías'), False, URL('config', 'users_membership'),[]),
     ]
    ),
]

acciones = [
    (T('Acciones'), False, None,
     [
         (T('Registros'), False, URL('action', 'registros'),[]),
         (T('Seguimiento'), False, URL('action', 'seguimiento'),[]),
     ]
    ),
]

atenciones = [
    (T('Atenciones'), False, URL('attention', 'index'),
     [
         (T('Registros'), False, URL('attention', 'registros'),[]),
         (T('Seguimiento'), False, URL('attention', 'seguimiento'),[]),
     ]
    ),
]

response.menu = home

if auth.user:
    if auth.has_membership(user_id=auth.user.id, role='administrador'):
        response.menu += config
        response.menu += acciones
        response.menu += atenciones
    elif auth.has_membership(user_id=auth.user.id, role='operador'):
        response.menu += acciones
    elif auth.has_membership(user_id=auth.user.id, role='usuario'):
        response.menu += atenciones