# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

@auth.requires_login()
def index():
    if auth.has_membership(user_id=auth.user.id, role='administrador'):
        redirect(URL('config','index'))
    elif auth.has_membership(user_id=auth.user.id, role='operador'):
        redirect(URL('action','index'))
    else:
        redirect(URL('attention','index'))
    return dict()

def error():
    return dict()

