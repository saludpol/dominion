# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def tipos_documentos():
    form = SQLFORM.grid(db.tipos_documentos, deletable=False)
    return dict(form=form)

def tipos_usuarios():
    form = SQLFORM.grid(db.tipos_usuarios, deletable=False)
    return dict(form=form)

def tipos_servicios():
    form = SQLFORM.grid(db.tipos_servicios, deletable=False)
    return dict(form=form)

def departamentos():
    form = SQLFORM.grid(db.departamentos, deletable=False)
    return dict(form=form)

def provincias():
    form = SQLFORM.grid(db.provincias, deletable=False)
    return dict(form=form)

def distritos():
    form = SQLFORM.grid(db.distritos, deletable=False)
    return dict(form=form)

#@auth.requires(restrictions)
@auth.requires_membership('administrador')
def users_manage():
    form = SQLFORM.grid(db.auth_user,
        csv=False, deletable=False)
    return dict(form=form)


#@auth.requires(restrictions)
@auth.requires_membership('administrador')
def users_groups():
    form = SQLFORM.grid(db.auth_group,
        csv=False, deletable=False)
    return dict(form=form)


#@auth.requires(restrictions)
@auth.requires_membership('administrador')
def users_membership():
    form = SQLFORM.grid(db.auth_membership,
        csv=False, deletable=False)
    return dict(form=form)