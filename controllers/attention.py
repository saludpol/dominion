# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def registros():
    tipo = request.vars.tipo

    user = auth.user.id
    form = SQLFORM.factory(
                Field('tipo_operacion', label=T('Tipo Operacion'), default=tipo, requires=IS_IN_SET(tipos_operaciones)),
                Field('resumen', 'string', label=T('Resumen')),
                Field('contenido', 'text', label=T('Contenido')),
    )
    if form.process().accepted:
        data = {}
        data['registrante'] = user
        data['tipo_operacion'] = form.vars.tipo_operacion
        data['resumen'] = form.vars.resumen
        data['contenido'] = form.vars.contenido
        db.operaciones.insert(**data)
    return dict(form=form)

def seguimiento():
    user = auth.user.id
    query = (db.operaciones.registrante==user)
    db.operaciones_acciones.registrante.default = user
    db.operaciones_acciones.registrante.writable = False
    db.operaciones_acciones.operacion.writable = False
    conditional = {'operaciones':query}
    form = SQLFORM.smartgrid(db.operaciones, linked_tables=['operaciones_acciones'],
                             deletable=False, editable=False,constraints=conditional,
                             fields=[
                                    db.operaciones.id,
                                    db.operaciones.registro, db.operaciones.registrante,
                                     db.operaciones.tipo_operacion, db.operaciones.referencia_nombre,
                                     db.operaciones.referencia_area, db.operaciones.referencia_area,
                                     db.operaciones.resumen,
                                     db.operaciones.contenido, db.operaciones.archivo,
                                     db.operaciones.estado,
                                     db.operaciones_acciones.registro, db.operaciones_acciones.registrante,
                                     db.operaciones_acciones.contenido, db.operaciones_acciones.area,
                                     db.operaciones_acciones.contacto
                                     ]
    )
    return dict(form=form)
