# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def registros():
    user = auth.user.id
    db.operaciones.registrante.default = user
    db.operaciones.registrante.writable = False
    form = SQLFORM.grid(db.operaciones, deletable=False)
    return dict(form=form)

def seguimiento():
    user = auth.user.id
    db.operaciones.registrante.default = user
    db.operaciones.registrante.writable = False
    db.operaciones_acciones.registrante.default = user
    db.operaciones_acciones.registrante.writable = False
    form = SQLFORM.smartgrid(db.operaciones, linked_tables=['operaciones_acciones'], deletable=False,
         fields=[
                db.operaciones.id,
                db.operaciones.registro, db.operaciones.registrante,
                 db.operaciones.tipo_operacion,
                 db.operaciones.origen_documento,
                 db.operaciones.origen_numero_documento,
                 db.operaciones.origen_tipo_usuario,
                 db.operaciones.origen_nombre,
                 db.operaciones.tipo_servicio,
                 db.operaciones.origen_telefono_fijo,
                 db.operaciones.resumen,
                 db.operaciones.contenido,
                 db.operaciones.estado,
                 db.operaciones_acciones.registro, db.operaciones_acciones.registrante,
                 db.operaciones_acciones.contenido, db.operaciones_acciones.area,
                 db.operaciones_acciones.contacto
                 ]
    )
    return dict(form=form)
